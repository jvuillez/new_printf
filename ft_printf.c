/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <jvuillez@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/05 15:38:58 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 12:31:04 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

/*
**	%[flags][width][.precision][length]specifier
**
**	%[flags]		-	;	+	;	~space	;	#	;	0
**	[width]			~number		;	*
**	[.precision]	~.number	;	.*
**	[length]		modify the specifier
**					hh	;	h	;	l	;	ll	;	j	;	z	;	t	;	L
**		42			hh	;	h	;	l	;	ll	;	j	;	z
** specifier	% s   p d   i o   u   x X c   f F e E g G a A n
** specifier42	% s S p d D i o O u U x X c C
*/

char	*ft_strjoin_free(char *s1, char *s2, int witch)
{
	char	*s1_start;
	char	*s2_start;
	char	*str;
	int		size;
	int		i;

	i = 0;
	s1_start = s1;
	s2_start = s2;
	size = ft_strlen(s1) + ft_strlen(s2) + 1;
	if (!(str = malloc(sizeof(char) * size)))
		return (NULL);
	while (*s1 != '\0')
		str[i++] = *s1++;
	while (*s2 != '\0')
		str[i++] = *s2++;
	str[i] = '\0';
	if (witch == 1 || witch == 2)
		free(s1_start);
	if (witch == 2 || witch == 3)
		free(s2_start);
	return (str);
}

int		ft_free_list(t_elem *start)
{
	t_elem		*tmp;
	t_elem		*to_free;

	tmp = start;
	while (tmp->next != NULL)
	{
		to_free = tmp;
		tmp = tmp->next;
		if (to_free->format->to_print != NULL)
			free(to_free->format->to_print);
		free(to_free->str);
		free(to_free->format);
		free(to_free);
	}
	free(tmp);
	return (-1);
}

int		ft_put_printf(t_elem *start)
{
	int			tot;
	t_elem		*tmp;

	tot = 0;
	tmp = start;
	while (tmp->next != NULL)
	{
		if (tmp->format->tot == -1)
			return (ft_free_list(start));
		if (tmp->next->next != NULL)
		{
			if (tmp->next->format->tot == -1)
				return (ft_free_list(start));
		}
		if (tmp->str[0] == '%')
		{
			tmp->format->ft_ptr(tmp->format);
			tot += tmp->format->tot;
		}
		else
			tot += ft_putstr(tmp->str);
		tmp = tmp->next;
	}
	ft_free_list(start);
	return (tot);
}

int		ft_printf(const char *format, ...)
{
	va_list		ap;
	t_elem		*start;
	t_elem		*tmp;

	va_start(ap, format);
	start = ft_lexer(format);
	tmp = start;
	while (tmp->next != NULL)
	{
		if (tmp->str[0] == '%')
			ft_parser(tmp->str + 1, ap, tmp->format);
		tmp = tmp->next;
	}
	return (ft_put_printf(start));
}
