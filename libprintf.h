/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libprintf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <jvuillez@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/05 16:20:35 by jvuillez          #+#    #+#             */
/*   Updated: 2016/03/10 17:07:53 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBPRINTF_H
# define LIBPRINTF_H

# define MOINS 1
# define PLUS 2
# define SPACE 4
# define DIESE 8
# define ZERO 16

# include "./libft/libft.h"

# include <locale.h>
# include <wchar.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <stdbool.h>

typedef struct		s_format{
	void			*data;
	void			(*ft_ptr)(struct s_format *f);
	bool			f;
	int				flag;
	bool			w;
	int				width;
	bool			p;
	int				prec;
	int				length;
	char			base[17];
	char			sign;
	int				sp;
	int				tot;
	char			*to_print;
}					t_format;

typedef struct		s_elem{
	struct s_format	*format;
	char			*str;
	struct s_elem	*next;
}					t_elem;

/*
** ft_cast_nbr_to_str_optional.c
*/

void				ft_print_nbr_intmax_t(t_format *f);
void				ft_print_nbr_size_t(t_format *f);

/*
** ft_cast_nbr_to_str.c
*/

void				ft_print_nbr_char(t_format *f);
void				ft_print_nbr_short(t_format *f);
void				ft_print_nbr_int(t_format *f);
void				ft_print_nbr_long(t_format *f);
void				ft_print_nbr_longlong(t_format *f);

/*
** ft_get_format.c
*/

int					ft_flags(char *str, t_format *f);
int					ft_width(char *str, t_format *f, va_list ap);
int					ft_precision(char *str, t_format *f, va_list ap);
int					ft_get_length(char *str, t_format *f);
char				*ft_get_format(char *str, t_format *f, va_list ap);

/*
** ft_lexer.c
*/

int					ft_true_char(char c);
int					ft_specifier(char c);
unsigned int		ft_strlen_next(char *str, unsigned int i);
t_elem				*ft_lexer(const char *format);

/*
** ft_parser.c
*/

void				ft_init_ftab(void (*tab_ft[127])(t_format *f));
void				ft_length_modifier(t_format *f);
void				ft_parser(char *str, va_list ap, t_format *f);

/*
** ft_print_char.c
*/

void				ft_print_null_c(t_format *f);
void				ft_print_c(t_format *f);
void				ft_print_bigc(t_format *f);
void				ft_bigc(t_format *f);

/*
** ft_print_nbr.c
*/

void				ft_init_ftab_nbr(void (*tab_ft[127])(t_format *f));
void				ft_print_nbr_p(t_format *f);
char				*ft_ulltoa_base(unsigned long long nb, char *base);

/*
** ft_print_nbr_x.c
*/

void				ft_print_nbr_o(t_format *f);
void				ft_print_nbr_u(t_format *f);
void				ft_print_nbr_di(t_format *f);
void				ft_print_nbr_bigx(t_format *f);
void				ft_print_nbr_x(t_format *f);

/*
** ft_print_str.c
*/

int					ft_putstr_free(char *str);
void				ft_print_bigs(t_format *f);
void				ft_bigs(t_format *f);
void				ft_print_s(t_format *f);

/*
** ft_printf.c
*/

char				*ft_strjoin_free(char *s1, char *s2, int witch);
int					ft_free_list(t_elem *start);
int					ft_put_printf(t_elem *start);
int					ft_printf(const char *format, ...);

/*
** ft_printspe.c
*/

void				ft_classic_print(t_format *f);
void				ft_add_prec(char first, t_format *f);
void				ft_decrease_width(t_format *f, int how_much);
void				ft_add_width(char first, char last, t_format *f);
char				ft_get_sign(t_format *f);

/*
** ft_unicode.c
*/

int					ft_what_the_size(wchar_t value);
int					ft_n_ustrlen(wchar_t *wstr, int n);
int					ft_print_unicode(\
					unsigned char *octet, size_t size, wchar_t value);
int					ft_putwchar(wchar_t v);

#endif
