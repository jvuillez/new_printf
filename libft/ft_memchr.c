/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 21:36:28 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/15 21:38:46 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char		uc;
	const unsigned char	*sc;

	sc = (const unsigned char *)s;
	uc = (unsigned char)c;
	while (n > 0)
	{
		if (*sc == uc)
			return ((void *)sc);
		sc++;
		n--;
	}
	return (NULL);
}
