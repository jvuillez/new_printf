/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 21:33:17 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/15 21:35:31 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memmove(void *s1, const void *s2, size_t n)
{
	char	*str1;
	char	*str2;

	str1 = (char*)s1;
	str2 = (char*)s2;
	if (s2 == s1)
		return (s1);
	if (s2 > s1)
		ft_memcpy(s1, s2, n);
	else
	{
		while (n > 0)
		{
			n--;
			str1[n] = str2[n];
		}
	}
	return (s1);
}
