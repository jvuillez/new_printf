/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/18 18:50:41 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/19 17:23:12 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_intlen(int n)
{
	int		i;

	i = 1;
	if (n == 0)
		return (2);
	while (n)
	{
		n = n / 10;
		i++;
	}
	return (i);
}

char			*ft_itoa(int n)
{
	char	*str;
	int		len;

	len = ft_intlen(n);
	str = ft_strnew(len);
	if (n == -2147483648)
		return (ft_strcpy(str, "-2147483648"));
	if (n == 0)
		str[0] = '0';
	len--;
	if (n < 0)
	{
		str[0] = '-';
		n = n * -1;
	}
	else
		len--;
	while (n)
	{
		str[len] = ((n % 10) + 48);
		n = n / 10;
		len--;
	}
	return (str);
}
