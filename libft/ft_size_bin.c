/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_size_bin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 15:46:48 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/14 15:51:39 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int		ft_size_bin(wchar_t value)
{
	unsigned int	size;

	size = 0;
	while (value > 0)
	{
		value = value / 2;
		size++;
	}
	return (size);
}
