Implementation de la fonction C printf
realisee dans le cardre du cursus 42.

L'accent a surtout ete mis sur l'architecture logicielle
le code a ete pense pour etre facilement lu, entretenu et ameliore.
Le plus gros probleme est l'optimisation : 16% des performances
de la fonction originale (pour une duree donnee) ; par ailleurs il manque
certaines fonctionnalites, notamment le %f pour les floats.

Pour lire le code :
Commencez par ft_printf.c
la fonction ft_printf() appelle d'abord un lexer
(voir ft_lexer.c) pour decortiquer les parametres (const char *format, ...)
les tokens que le lexer retourne sont soit stockes prets a etre imprimes directement
soit traites par un parser (voir ft_parser.c et ft_get_format.c) si le token commence par '%'
puis stockees prets à etre imprimes dans la plupart des cas graces aux fonctions de
ft_print_char.c
ft_print_string.c
ft_cast_nbr_to_str_optional.c ; ft_print_nbr_x.c ; ft_cast_nbr_to_str.c ; ft_print_nbr.c

Et revenez a ft_printf.c : la fontion ft_put_printf() write les tokens un par un,
et gere aussi les cas d'erreurs.