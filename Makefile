# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/04/19 23:42:33 by jvuillez          #+#    #+#              #
#    Updated: 2017/12/28 18:10:43 by jvuillez         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

CFLAGS = -Wall -Werror -Wextra

NAME = libftprintf.a

OBJ_O = libft/ft_atoi.o \
		libft/ft_bzero.o \
		libft/ft_isalnum.o \
		libft/ft_isalpha.o \
		libft/ft_isascii.o \
		libft/ft_isdigit.o \
		libft/ft_isprint.o \
		libft/ft_itoa.o \
		libft/ft_memalloc.o \
		libft/ft_memccpy.o \
		libft/ft_memchr.o \
		libft/ft_memcmp.o \
		libft/ft_memcpy.o \
		libft/ft_memdel.o \
		libft/ft_memmove.o \
		libft/ft_memset.o \
		libft/ft_nb_length.o \
		libft/ft_putchar.o \
		libft/ft_putchar_fd.o \
		libft/ft_putendl.o \
		libft/ft_putendl_fd.o \
		libft/ft_putnbr.o \
		libft/ft_putnbr_fd.o \
		libft/ft_putstr.o \
		libft/ft_putnstr.o \
		libft/ft_putstr_fd.o \
		libft/ft_strspam.o \
		libft/ft_strcat.o \
		libft/ft_strchr.o \
		libft/ft_strclr.o \
		libft/ft_strcmp.o \
		libft/ft_strcpy.o \
		libft/ft_strdel.o \
		libft/ft_strdup.o \
		libft/ft_strndup.o \
		libft/ft_strequ.o \
		libft/ft_striter.o \
		libft/ft_striteri.o \
		libft/ft_strjoin.o \
		libft/ft_strlcat.o \
		libft/ft_strlen.o \
		libft/ft_strlen_char.o \
		libft/ft_strmap.o \
		libft/ft_strmapi.o \
		libft/ft_strncat.o \
		libft/ft_strnclone.o \
		libft/ft_strncmp.o \
		libft/ft_strncpy.o \
		libft/ft_strnequ.o \
		libft/ft_strnew.o \
		libft/ft_strnstr.o \
		libft/ft_strrchr.o \
		libft/ft_strsplit.o \
		libft/ft_strstr.o \
		libft/ft_strsub.o \
		libft/ft_strtrim.o \
		libft/ft_size_bin.o \
		libft/ft_tolower.o \
		libft/ft_toupper.o \
		libft/ft_ustrlen.o \
		ft_printf.o \
		ft_lexer.o \
		ft_parser.o \
		ft_get_format.o \
		ft_printspe.o \
		ft_cast_nbr_to_str.o \
		ft_cast_nbr_to_str_optional.o \
		ft_print_nbr.o \
		ft_print_nbr_x.o \
		ft_print_str.o \
		ft_print_char.o \
		ft_unicode.o

all: $(NAME)
	make -C libft/

$(NAME): $(OBJ_O)
	ar rc $(NAME) $(OBJ_O)
	ranlib $(NAME)

%.o: %.c
	$(CC) -o $@ -c $^ $(CFLAGS)

clean:
	rm -f $(OBJ_O)
	make -C libft/ clean

fclean: clean
	rm -f $(NAME)
	make -C libft/ fclean

re: fclean all
	make -C libft/ re

er: all clean
