/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_str.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 15:15:25 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/28 15:15:27 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

int			ft_putstr_free(char *str)
{
	int		i;

	i = ft_strlen(str);
	write(1, str, ft_strlen(str));
	free(str);
	return (i);
}

void		ft_print_bigs(t_format *f)
{
	wchar_t		*wstr;
	wchar_t		w;
	char		to_spam;
	wchar_t		max;
	wchar_t		n;

	wstr = (wchar_t*)f->data;
	to_spam = ' ';
	if ((f->flag & ZERO) != 0)
		to_spam = '0';
	max = ft_ustrlen(wstr);
	if (f->p == 1 && max > ft_n_ustrlen(wstr, f->prec))
		max = ft_n_ustrlen(wstr, f->prec);
	f->tot += ft_putstr_free(ft_strspam(to_spam, f->width - max));
	n = 0;
	w = 0;
	while (ft_what_the_size(wstr[w]) + n <= max)
		n += ft_putwchar(wstr[w++]);
	f->tot += n;
	f->tot += ft_putstr_free(ft_strspam(to_spam, -f->width - max));
}

void		ft_bigs(t_format *f)
{
	wchar_t	*wstr;
	int		i;

	i = 0;
	if (f->data == NULL)
		f->to_print = ft_strjoin_free(f->to_print, ft_strdup("(null)"), 2);
	else
	{
		wstr = (wchar_t*)f->data;
		while (wstr[i] != '\0')
		{
			if (wstr[i] <= -1 || wstr[i] >= 1114111)
				f->tot = -1;
			else if (wstr[i] >= 55296 && wstr[i] <= 57343)
				f->tot = -1;
			i++;
		}
		f->ft_ptr = &ft_print_bigs;
	}
}

void		ft_print_s(t_format *f)
{
	char		to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0)
		to_spam = '0';
	free(f->to_print);
	if (f->data == NULL)
	{
		if (f->p == 1)
			f->to_print = ft_strndup("(null)", f->prec);
		else
			f->to_print = ft_strdup("(null)");
	}
	else if (f->p == 1)
		f->to_print = ft_strndup((char*)f->data, f->prec);
	else
		f->to_print = ft_strdup((char*)f->data);
	ft_add_width(to_spam, ' ', f);
	if (f->prec < 0)
		ft_add_prec(' ', f);
}
